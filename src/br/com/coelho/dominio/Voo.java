package br.com.coelho.dominio;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 

public class Voo {
	private String origem;
	private String destino;
	private Date data;
	private Date horario;
        private List<Assento> assentos =  new ArrayList<Assento>();

    public Voo(String origem, String destino, Date data, Date horario) {
        this.origem = origem;
        this.destino = destino;
        this.data = data;
        this.horario = horario;
        
    }

        
        
    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public List<Assento> getAssentos() {
        return assentos;
    }

    public void setAssentos(List<Assento> assentos) {
        this.assentos = assentos;
    }

    public void adicionaAssento(TipoAssento tipoAssento, int quantidade) {
      
        for(int i=0; i<quantidade;i++){
               Assento assento = new Assento();
               assento.setTipoAssento(tipoAssento);
               assentos.add(assento);
        }
    }

    public Assento getAssento(TipoAssento tipoAssento) {
        for(int i=0; i<assentos.size(); i++){
            Assento assento = assentos.get(i);
            if (assento.getTipoAssento().equals(tipoAssento)) {
                assentos.remove(assento);
                return assento;
            }
        }
        return null;
    }

    
        
        
}
