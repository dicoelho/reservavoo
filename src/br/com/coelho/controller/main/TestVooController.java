/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.coelho.controller.main;

import br.com.coelho.controller.VooController;
import br.com.coelho.dominio.TipoAssento;

/**
 *
 * @author estagiario10
 */
public class TestVooController {
    
    public static void main(final String[] args) {
        VooController vooController = new VooController();
        
        for (int i = 0; i <14; i++) {
            vooController.disponibilidadeAssento(TipoAssento.VIP);    
        }

        for (int i = 0; i <54; i++) {
            vooController.disponibilidadeAssento(TipoAssento.ECONOMICO);
        }
        
    }
    
}
